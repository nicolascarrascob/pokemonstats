<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <link rel="stylesheet" href="../css/bootstrap.min.css">   		
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css">   		
        <script src="../js/bootstrap.min.js"></script>  
        <script src="//code.jquery.com/jquery-1.12.3.js"></script> 
        <script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script> 
        <script src="https://cdn.datatables.net/1.10.12/js/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript">
        $(document).ready(function() {
		    $('#pokemones').DataTable();
		} );
        </script>
    </head>

    <body>          
        <div class="container">
            <h2>Pokemon Stats</h2>
            <!--Search Form -->
            <form action="/pokemon" method="get" id="seachEmployeeForm" role="form">	            
	            <div>
	            <span>Instrucciones</span>
	            <ul>
	            	<li>Entrar en <a target="_blank" href="https://accounts.google.com/o/oauth2/auth?client_id=848232511240-73ri3t7plvk96pj4f85uj8otdat2alem.apps.googleusercontent.com&redirect_uri=urn%3Aietf%3Awg%3Aoauth%3A2.0%3Aoob&response_type=code&scope=openid%20email%20https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fuserinfo.email">Link para Auth Key</a></li>
	            	<li>Dar acceso a pokemon go para obtener una key de 1 uso</li>
	            	<li>Copiar la Key en el formulario y darle a buscar</li>
	            </ul>
	            	
	            </div>
                <input type="hidden" id="useProxy" name="useProxy" value="true">
                <div class="form-group col-xs-5">
                    <input type="text" name="authKey" id="authKey" class="form-control" required="true" placeholder="Auth Key"/>                    
                </div>
                <button type="submit" class="btn btn-info">
                    <span class="glyphicon glyphicon-search"></span> Buscar
                </button>
                <br></br>
                <br></br>
            </form>
            
            <form action="/pokemon" method="get" id="seachEmployeeForm" role="form">	            
	            <div>
	            <span>Instrucciones</span>
	            <ul>
	            	<li>Acceso con cuenta pokemon</li>
	            </ul>
	            	
	            </div>
                <input type="hidden" id="useProxy" name="useProxy" value="true">
                <div class="form-group col-xs-5">
                    <input type="text" name="user" id="user" class="form-control" required="true" placeholder="user"/> 
                    <input type="text" name="pass" id="pass" class="form-control" required="true" placeholder="pass"/>                       
                </div>
                <button type="submit" class="btn btn-info">
                    <span class="glyphicon glyphicon-search"></span> Buscar
                </button>
                <br></br>
                <br></br>
            </form>

            <!--Employees List-->
            <c:if test="${not empty mensaje}">                
                <div class="alert alert-success">
                    ${mensaje}
                </div>
            </c:if>           
                <c:choose>
                    <c:when test="${not empty pokemonList}">
                        <table id="pokemones" class="table table-striped">
                            <thead>
                                <tr>
                                    <td>Nombre</td>
                                    <td>% Perfecto</td>
                                    <td>CP</td>
                                    <td>Ataque</td>
                                    <td>Defensa</td>
                                    <td>Estamina</td>
                                    <td>Nivel</td>
                                </tr>
                            </thead>
                            <c:forEach var="pokemon" items="${pokemonList}">
                                <tr >                                  
                                    <td>${pokemon.nombre}</td>
                                    <td>${pokemon.perfecto}</td>
                                    <td>${pokemon.cp}</td>
                                    <td>${pokemon.ataque}</td>
                                    <td>${pokemon.defensa}</td>
                                    <td>${pokemon.estamina}</td>
                                    <td>${pokemon.nivel}</td>
                                </tr>
                            </c:forEach>               
                        </table>  
                    </c:when>                    
                    <c:otherwise>
                        <br>           
                        <div class="alert alert-info">
                            No tiene pokemon
                        </div>
                    </c:otherwise>
                </c:choose>   
        </div>
    </body>
</html>