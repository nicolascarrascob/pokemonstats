package com.nicolascarrascob.pokemonstats;

public class Pokemon {
	
	private String nombre;	
	private String perfecto;
	private String cp;
	private String ataque;
	private String defensa;
	private String estamina;
	private String nivel;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getPerfecto() {
		return perfecto;
	}
	public void setPerfecto(String perfecto) {
		this.perfecto = perfecto;
	}
	public String getCp() {
		return cp;
	}
	public void setCp(String cp) {
		this.cp = cp;
	}
	public String getAtaque() {
		return ataque;
	}
	public void setAtaque(String ataque) {
		this.ataque = ataque;
	}
	public String getDefensa() {
		return defensa;
	}
	public void setDefensa(String defensa) {
		this.defensa = defensa;
	}
	public String getEstamina() {
		return estamina;
	}
	public void setEstamina(String estamina) {
		this.estamina = estamina;
	}
	public String getNivel() {
		return nivel;
	}
	public void setNivel(String nivel) {
		this.nivel = nivel;
	}
	
	
	

}
