package com.nicolascarrascob.pokemonstats;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import okhttp3.OkHttpClient;

import com.pokegoapi.api.PokemonGo;
import com.pokegoapi.api.inventory.Inventories;
import com.pokegoapi.api.pokemon.Pokemon;
import com.pokegoapi.auth.CredentialProvider;
import com.pokegoapi.auth.GoogleUserCredentialProvider;
import com.pokegoapi.auth.PtcCredentialProvider;
import com.pokegoapi.util.PokeNames;

/**
 * Servlet implementation class PokemonServlet
 */
@WebServlet(
        name = "PokemonServlet",
        urlPatterns = {"/pokemon"}
)
public class PokemonServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PokemonServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		
		String authKey = request.getParameter("authKey");
		String user = request.getParameter("user");
		String pass = request.getParameter("pass");
		List<com.nicolascarrascob.pokemonstats.Pokemon> pokemonList = new ArrayList<com.nicolascarrascob.pokemonstats.Pokemon>();
			String useProxy = request.getParameter("useProxy");
			OkHttpClient http;
			if("true".equals(useProxy)){
				http = new OkHttpClient.Builder()
			    .connectTimeout(60, TimeUnit.SECONDS)
			    .writeTimeout(60, TimeUnit.SECONDS)
			    .readTimeout(60, TimeUnit.SECONDS)
			    .proxy(new Proxy(Proxy.Type.HTTP, new InetSocketAddress("10.214.8.100", 8080)))
			    .build();
			}else{
				http = new OkHttpClient.Builder()
			    .connectTimeout(60, TimeUnit.SECONDS)
			    .writeTimeout(60, TimeUnit.SECONDS)
			    .readTimeout(60, TimeUnit.SECONDS)
			    .build();
			}
			
			try {
				CredentialProvider provider = null;
				if(authKey != null){
					// instanciate a provider, it will give an url
					provider = new GoogleUserCredentialProvider(http);
					// in this url, you will get a code for the google account that is logged
					String access = authKey;
					
					// we should be able to login with this token
					((GoogleUserCredentialProvider) provider).login(access);
				}else if(user != null && pass != null){
					provider = new PtcCredentialProvider(http, user, pass);
				}else{
					forwardListPokemon(request, response, pokemonList);
					return;
				}
								
				PokemonGo go = new PokemonGo(provider, http);

				Inventories inventories =  go.getInventories(); // to get all his inventories (Pokemon, backpack, egg, incubator)
				
				Collections.sort(inventories.getPokebank().getPokemons(), new Comparator<Pokemon>() {
					  public int compare(Pokemon o1, Pokemon o2) {
					      return o2.getIvRatio() == o1.getIvRatio() ? 0 :  (o2.getIvRatio() > o1.getIvRatio() ? 1 : -1);
					  }
				});				
				
				com.nicolascarrascob.pokemonstats.Pokemon pokemon2;
				for(Pokemon pokemon: inventories.getPokebank().getPokemons()){
					System.out.println(PokeNames.getDisplayName(pokemon.getPokemonId().getNumber(), Locale.ENGLISH) + " Perfecto:" + 
				String.format("%.2f%%",pokemon.getIvRatio()*100) + " CP: " + pokemon.getCp() +
				" A: " + pokemon.getIndividualAttack() + " D: " + pokemon.getIndividualDefense() + " S: " + pokemon.getIndividualStamina());
					pokemon2 = new com.nicolascarrascob.pokemonstats.Pokemon();
					pokemon2.setNombre(PokeNames.getDisplayName(pokemon.getPokemonId().getNumber(), Locale.ENGLISH));
					pokemon2.setPerfecto(String.format("%.2f%%",pokemon.getIvRatio()*100));
					pokemon2.setCp(String.valueOf(pokemon.getCp()));
					pokemon2.setAtaque(String.valueOf(pokemon.getIndividualAttack()));
					pokemon2.setDefensa(String.valueOf(pokemon.getIndividualDefense()));
					pokemon2.setEstamina(String.valueOf(pokemon.getIndividualStamina()));
					pokemon2.setNivel(String.valueOf(pokemon.getLevel()));
					pokemonList.add(pokemon2);
				}
				
				forwardListPokemon(request, response, pokemonList);
				
			} catch (Exception e) {
				e.printStackTrace();
				request.setAttribute("mensaje", e.getLocalizedMessage());
				forwardListPokemon(request, response, pokemonList);
			}
		
	}
	
	private void forwardListPokemon(HttpServletRequest req, HttpServletResponse resp, List pokemonList)
            throws ServletException, IOException {
        String nextJSP = "/jsp/list-pokemon.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        req.setAttribute("pokemonList", pokemonList);
        dispatcher.forward(req, resp);
    }   

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
